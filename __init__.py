# card10 Joust
# based on https://github.com/adangert/JoustMania

import display
import os
import utime
import buttons
import color
import leds
import vibra
import bhi160

ACCELEROMETER = bhi160.BHI160Accelerometer()
class JoustSensor:
    NUM_READINGS = 1
    def __init__(self):
        self.sensor = ACCELEROMETER
        self.last_accels = []
        self.jerk = 0.0
        self.threshold = 1.8
        self.warn_threshold = 1.4
    def get_jerk(self):
        readings = list()
        while not readings:
            readings = self.sensor.read()
            if len(readings) > 0:
                self.last_accels = self.last_accels + readings
                self.last_accels = self.last_accels[-JoustSensor.NUM_READINGS:]
                jerk_x = sum([a.x for a in self.last_accels]) / JoustSensor.NUM_READINGS
                jerk_y = sum([a.y for a in self.last_accels]) / JoustSensor.NUM_READINGS
                jerk_z = sum([a.z for a in self.last_accels]) / JoustSensor.NUM_READINGS
                self.jerk = (jerk_x**2 + jerk_y**2 + jerk_z**2)**0.5
        return self.jerk
    def too_fast(self):
        return self.jerk > self.threshold
    def warning(self):
        return self.jerk > self.warn_threshold

def wait_button():
    utime.sleep_ms(1000)
    while True:
        pressed = buttons.read(
            buttons.BOTTOM_LEFT | buttons.BOTTOM_RIGHT
        )
        if pressed != 0:
            break
    if pressed & buttons.BOTTOM_LEFT != 0:
        return "BL"
    if pressed & buttons.BOTTOM_RIGHT != 0:
        return "BR"
    if pressed & buttons.UPPER_RIGHT != 0:
        return "BR"
    if pressed & buttons.UPPER_LEFT != 0:
        return "UL"
    return "??"

def joining_game():
    set_display("JOIN ?", color.WHITE)
    set_color(color.WHITE)

def status_in_game():
    set_display("IN GAME", color.GREEN)
    set_color(color.GREEN)

def status_out_game():
    set_display("DROPPED", color.RED)
    set_color(color.RED)
    vibra.vibrate(1000)

def status_warning():
    set_display("IN GAME", color.YELLOW)
    set_color(color.YELLOW)

def set_display(text, colr):
    with display.open() as disp:
        disp.clear()
        disp.print(text, fg=colr, bg=(0,0,0), posx=30, posy=30)
        disp.update()

def set_color(colr):
    for i in range(15):
        leds.prep(i, colr)
    leds.update()

def countdown(n):
    colr = color.WHITE
    for i in range(n,0,-1):
        if i == 3: colr = color.RED
        elif i == 2: colr = color.YELLOW
        elif i == 1: colr = color.GREEN
        set_display("START: %d" % i, colr)
        leds.clear()
        vibra.vibrate(300)
        utime.sleep_ms(1000)
    vibra.vibrate(1000)

def run():
    print("Starting ...")
    acc = JoustSensor()
    i=0
    states = [joining_game, status_in_game, status_warning, status_out_game]
    while True:
        current_status = states[i%len(states)]
        current_status()
        i+=1
        if current_status == joining_game:
            wait_button()
            countdown(5)
        elif current_status == status_in_game:
            while True:
                acc.get_jerk()
                if acc.warning():
                    vibra.set(True)
                    break
        elif current_status == status_warning:
            while True:
                if not acc.warning():
                    vibra.set(False)
                    i = states.index(status_in_game)
                    break
                if acc.too_fast():
                    break
                acc.get_jerk()
        elif current_status == status_out_game:
            wait_button()

run()
